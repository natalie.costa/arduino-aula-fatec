int led0 = 8;
int led1 = 9;
int led2 = 10;
int bt_up = 3;
int bt_down = 2;
float luminosidadeLED = 0;
int pinoLED = 12;

int acum1 = 0;
int cont1 = 0;

int potenc = A0;
int valor_potenc;

void setup() {
  Serial.begin(9600);
  pinMode(led0, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(bt_up, INPUT_PULLUP);
  pinMode(bt_down, INPUT_PULLUP);
  pinMode(potenc, INPUT);
  pinMode(pinoLED, OUTPUT); 
}

void loop() {
  int estado_up = digitalRead(bt_up);
  Serial.print("Estado up = ");
  Serial.println(estado_up);
  
  //int estado_down = digitalRead(bt_down);
  //Serial.print("Estado down = ");
  //Serial.println(estado_down);

  if (estado_up = 1) {
    for (int i = 0; i < 10; i ++) {

      valor_potenc = analogRead(potenc); 
      Serial.print("Potenciometro = ");
      Serial.println(valor_potenc);
      delay(1000);
      
      luminosidadeLED = map(analogRead(potenc), 0, 1023, 0, 255);
      analogWrite(pinoLED, luminosidadeLED);

      acum1 += luminosidadeLED;
      cont1 ++; 

      Serial.print("Média = ");
      Serial.println(acum1/cont1);
    }
  }
}
