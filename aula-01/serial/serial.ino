float temperatura = 0; // for incoming serial data

void setup() {
  Serial.begin(9600); // opens serial port, sets data rate to 9600 bps
}

void loop() {
  // reply only when you receive data:
  if (Serial.available() > 0) {
    temperatura = Serial.parseFloat();
    
    if (temperatura >= 0 && temperatura <= 10) {
      Serial.print("Verde: ");
    }
    else if (temperatura >= 11 && temperatura <= 20){
      Serial.print("Amarelo: ");
    }
    else if (temperatura >= 21 && temperatura <= 30){
      Serial.print("Vermelho: ");
    }
    else {
      Serial.print("Invalido ");
    }
    // say what you got:
    Serial.println(temperatura);
  }
}
